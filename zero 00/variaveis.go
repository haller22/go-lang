package main

import "fmt"

func exibirInstanciarEstatic(  )  {

  // go tipagem estatico

  // declaracao de variavel
  var numero int

  // inicializacao de variavel
  numero = 25

  // exibicao de variavel
  fmt.Println( numero )

  numero = 40
  fmt.Println( numero )

}

func declaraInstanciaAltomaticamente(  )  {

  nome := "Eliseu"

  fmt.Println( nome )

  nome = "Pedro"

  fmt.Println( nome )

  // error !
  // nome = 40
  // nome := "Elisa"
  // declaracao e inicializacao de variavel
  // :=
}

func multInicializacao(  )  {

  // ## Error - falta declarar :=
  // nome, numero = "Rafael", 42

  nome, numero := "Rafael", 42

  fmt.Println( nome )
  fmt.Println( numero )
}

func trocaValores(  )  {

  nomeInicial := "Casa"
  nomeFinal := "de Papel"

  fmt.Println( nomeInicial )
  fmt.Println( nomeFinal )

  nomeInicial, nomeFinal = nomeFinal, nomeInicial

  fmt.Println( nomeInicial )
  fmt.Println( nomeFinal )
}

func multAtribuicao(  )  {

  segundoNome := "Jose"
  primeiroNome, segundoNome := "Maria", "Felipe"

  fmt.Println( primeiroNome )
  fmt.Println( segundoNome )
}

func criarSemUtilizar(  )  {

  var numeral int
  var letras string
  var natural bool

  fmt.Println( numeral )
  fmt.Println( letras )
  fmt.Println( natural )
}

func main(  )  {

  exibirInstanciarEstatic(  )
  declaraInstanciaAltomaticamente(  )
  multInicializacao(  )
  trocaValores(  )
  multAtribuicao(  )
  criarSemUtilizar(  )
}
