// todo programa em go deve comecar com a declaracao de um pacote
package main

// todo pacote e' importado com a palavra chave import e o nome do pacote entre aspas
import "fmt"

// Comentario uma Linha
// Ola Mundo!

/*
Comentario Multi Linha!
aaaaaaa

*/


func main(  )  {

  fmt.Println("Ola Mundo!")
}
